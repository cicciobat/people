import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, OnDestroy, OnChanges {
  isOpen: boolean = false;
  numbers: number[] = [1, 2, 3, 4, 5, 10, 90, 70];

  constructor() {
    this.isOpen = true;
    console.log('Construct');
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    }

  ngOnInit(): void {
    console.log('Ciao');
  }

  ngOnDestroy(): void {
    console.log('Arrivederci');
  }
}
