import { NomeCompleto } from './NomeCompleto';
import { DataDiNascita } from './DataDiNascita';

export interface Persona {
  nomeCompleto: NomeCompleto;
  dataDiNascita: DataDiNascita;
}
